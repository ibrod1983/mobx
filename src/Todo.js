
// import { observer } from "mobx-react"

export default ({todo,onNameChange,onDelete}) => {
    
    // debugger
    console.log('todo rendering')


    return (
        <div style={{padding:'10px',margin:'10px'}}>
            <p>ID: {todo.id}</p>
            <input onChange={(e)=>{onNameChange(todo.id,e.target.value)}}  value={todo.name}></input>
            <p>Completed: {todo.completed ? 'true' : 'false'} <button onClick={()=>{onDelete(todo.id)}} className="btn btn-danger">Delete</button></p>
            
        </div>
    )
}
