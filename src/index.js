import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import { createContext } from 'react';
import TodosStore from './stores/TodosStore';
import 'bootstrap/dist/css/bootstrap.min.css'
// import { autorun } from "mobx"



const todosStore = new TodosStore();
export const combinedStores = {todosStore,}

// autorun(() => {
//   console.log(calculatorStore.activeYearValCode)
// })


// export const StoresContext = createContext(combinedStores)


ReactDOM.render(
  <React.StrictMode>
    {/* <StoresContext.Provider value={combinedStores} > */}
      <App />
    {/* </StoresContext.Provider > */}
   </React.StrictMode>,
  
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
