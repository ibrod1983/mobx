import logo from './logo.svg';
import './App.css';
// import { inject } from "mobx-react"
// import Person from './Person'
import {combinedStores} from './index';
import Todos from './Todos'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { useState } from 'react';
import { observer } from 'mobx-react';





function App() {  
  
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/">          
            <Todos ></Todos>
          </Route>
        </Switch>
      </Router>

    </div>
  );
}


//  function App() {
//   // const {todosStore} =  combinedStores
//   // const useMobx = true;
//   // const [_todos, _setTodos] = useState(
//   //   [
//   //     {
//   //       id: 1,
//   //       name: "fsdfsdf",
//   //       completed: true
//   //     },
//   //     {
//   //       id: 15,
//   //       name: "dasd34214",
//   //       completed: true
//   //     },
//   //     {
//   //       id: 14,
//   //       name: "23234",
//   //       completed: true
//   //     },
//   //     {
//   //       id: 13,
//   //       name: "dasdas",
//   //       completed: true
//   //     },
//   //     {
//   //       id: 123,
//   //       name: "dasd",
//   //       completed: true
//   //     },
//   //   ]
//   // )
//   // debugger
//   console.log('app rendering')
//   // const _editTodoName = (id,value) => {
//   //   // debugger
//   //   // if(useMobx){
//   //   //   todosStore.editTodoName(id,value)
//   //   //   return;
//   //   // }
//   //   _setTodos(todos.map((todo) => {
//   //     if (todo.id === id) {
//   //       return {
//   //         ...todo,
//   //         name: value
//   //       }
//   //     } else {
//   //       return todo
//   //     }
//   //   }))
//   // }
//   // const todos = useMobx ? todosStore.todos : _todos
//   return (
//     <div className="App">
//       <Router>
//         <Switch>

//           <Route path="/">
//             {/* <Todos onTodoNameChange={(id, value) => {
//               _editTodoName(id,value)
//             }} todos={todos}></Todos> */}
//             {/* <Todos todosStore={todosStore}></Todos> */}
//             <Todos ></Todos>
//           </Route>

//         </Switch>
//       </Router>

//     </div>
//   );
// }

export default observer(App)
