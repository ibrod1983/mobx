
import { observer } from "mobx-react"
import { useContext, useEffect ,useCallback} from "react"
import { StoresContext } from "."
import Todo from './Todo'
import { combinedStores } from "."

const ObservableTodo = observer(Todo);

export default observer(() => {
    const { todosStore } = combinedStores
   

    const memoizedOnDelete = useCallback(
        (id) => {
            todosStore.removeTodo(id)
        },
        [todosStore.todos]
      );

      const memoizedOnNameChange= useCallback(
        (id,value) => {
            todosStore.editTodoName(id,value)
        },
        [todosStore.todos]
      );
    
    console.log('todoS rendering')
    return (
        <div >
            <button onClick={todosStore.addRandomTodo}>add</button>
            {todosStore.todos.map(todo=>{
                return(
                    // <Todo onNameChange={(value)=>{onTodoNameChange(todo.id,value)}} key={todo.id} todo={todo}></Todo>
                    // <ObservableTodo onDelete={()=>{todosStore.removeTodo(todo.id)}} onNameChange={(value)=>{todosStore.editTodoName(todo.id,value)}} key={todo.id} todo={todo}></ObservableTodo>
                    <ObservableTodo id={todo.id} onDelete={memoizedOnDelete} onNameChange={memoizedOnNameChange} key={todo.id} todo={todo}></ObservableTodo>
                    // <Todo onNameChange={onNameChange} key={todo.id} todo={todo}></Todo>
                )
            })}
        </div>
    )
})
