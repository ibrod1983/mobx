import { observable, makeAutoObservable, runInAction } from "mobx"
// import loginViaBackend from './services/login';
// import {login,logout} from './services/login'
// import logout from './services/logout'



export default class TodosStore {


    constructor() {
         /**
         * @type {Array<{name:string,id:string,completed:boolean}>}
         */    
        this.todos =   [
            {
                id: 1,
                name: "fsdfsdf",
                completed: true
            },
            {
                id: 15,
                name: "mobxxx42343",
                completed: true
            },
            {
                id: 14,
                name: "mobxxx",
                completed: true
            },
            {
                id: 13,
                name: "dasdas",
                completed: true
            },
            {
                id: 123,
                name: "dasd",
                completed: true
            },
        ]
        makeAutoObservable(this)
       


    }   


    /**
     * 
     * @param {number} id the id of the todo 
     * @param {string} value the new value of the todo name
     */
    editTodoName = (id, value) => {
        // debugger
        for(let todo of this.todos){
            if(todo.id === id){
                // debugger
                todo.name = value
            }
        }
        // this.todos = this.todos.map((todo) => {
        //     if (todo.id === id) {
        //         return {
        //             ...todo,
        //             name: value
        //         }
        //     } else {
        //         return todo
        //     }
        // })

    }


    /**
     * 
     * @param {number|string} id The todo id to remove 
     */
    removeTodo(id){
        const todo = this.todos.find(todo => todo.id === id)[0]
        this.todos.splice(this.todos.indexOf(todo), 1)

    }

    addRandomTodo = ()=>{
        this.todos.push({
            id:  '_' + Math.random().toString(36).substr(2, 9),
            name: "randomly added",
            completed: true
        },)
    }




    // async getTodos() {


    //     const todos = await API_getTodos();
    //     debugger
    //     runInAction(() => { this.todos = todos });

    // }



};


// async function API_getTodos() {
//     return [
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo1', completed: true },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo2', completed: true },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo3', completed: false },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo4', completed: true },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo5', completed: true },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo6', completed: false },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo7', completed: false },
//         { id: '_' + Math.random().toString(36).substr(2, 9), name: 'Some todo8', completed: true },
//     ];
// }


