import React, { Component } from 'react'
import './Style.css'
import Select from "../../components/select/select";
import Slider from '../../components/react-rangeslider';
import 'react-rangeslider/lib/index.css';
import '../../components/slider/slider';
import '../../components/slider/index.css';
import { Modal, Button } from "react-bootstrap";
import Finance from "tvm-financejs";
import Api from '../../utils/Api';
import Loader from '../../asserts/images/loader.gif';
import logo from '../../asserts/images/svgmobilelogo.svg';
import logoDesktop from '../../asserts/images/svgdesktoplogo.svg';
import { isMobile } from "react-device-detect";
class App extends Component {

    constructor(props, context, ...args) {
        super(props, context, ...args)
        this.state = {
            yearsList: [],
            rangePayList: [],

            selectValue: null,

            ribitLabel: null,
            ribit: null,
            prime: null,

            calculateSum: false,

            monthlyRepayments: null,

            loading: true,

            // first slider
            sumDealMin: null,
            sumDealMax: null,
            sumDealJump: null,
            sumDealSliderValue: null, //the first vaule of the slider - need to be in the range (managed)

            // second slider
            sumLoanDealMin: null,
            sumLoanDealJump: null,
            sumLoanDealSliderValue: null,

            // third slider
            numOfPaymentMin: null,
            numOfPaymentMax: null,
            numOfPaymentJump: 1,
            numOfPaymentValue: null,

            // check if display fourth slider
            minCarYear: null,
            minPay: null,
            maxPay: null,

            // fourth slider
            minPercentSlider: null,
            percentSliderValue: null,
            maxPercentSlider: null,

            modalVisible: null,
            modalShow: false,
            show: null,

            htmlText: null,
            privacyHtmlText: null,
            termsHtmlText: null,
            negishutHtmlText: null,
            modalTitle: null,

            dictionaryTextList: null,

            showDealSumSliderFocus: false,
            showLoanSumSliderFocus: false,
            showPaymentsSliderFocus: false,
            showBalloonPercentageSliderFocus: false,

            maxBalloonInput: null,
            endBalloonPeriod: null,

            showBalloonSlider: null
        }
    }

    componentDidMount() {
        console.log("componentDדגגדidMount Calculator")
        document.addEventListener('contextmenu', (e) => {
            e.preventDefault();
        });
        this.getData();
    }

    getData = async () => {
        try {
            let response = await Api.getInitData();
            let dictionaryResponse = await Api.getDictionaryItems();
            if (response.yearsList[0].isPrime === false) {
                if (response.yearsList[0].indexLinked === true) {
                    this.setState({ ribitLabel: dictionaryResponse[dictionaryResponse.findIndex(x => x.Key === "Ribit")].Value + response.yearsList[0].r + '%' + dictionaryResponse[dictionaryResponse.findIndex(x => x.Key === "IndexLinked")].Value })
                } else {
                    this.setState({ ribitLabel: dictionaryResponse[dictionaryResponse.findIndex(x => x.Key === "Ribit")].Value + response.yearsList[0].r + '%' })
                }
            } else {
                this.setState({ ribitLabel: dictionaryResponse[dictionaryResponse.findIndex(x => x.Key === "Prime")].Value + '+ ' + response.yearsList[0].differenceFromPrime + '%' })
            }

            this.setState({
                yearsList: response.yearsList,
                // rangePayList: response.rangePayList,
                selectValue: { value: response.yearsList[0].valCode, label: response.yearsList[0].valName },
                minCarYear: response.minYearForBaloon,
                ribit: response.yearsList[0].r / 100,
                prime: response.prime,
                percentSliderValue: response.yearsList[0].baloonArray[response.yearsList[0].baloonArray.length - 1].baloonRangePercentage,
                // maxPercentSlider: response.rangePayList[response.rangePayList.length-1].baloonRangePercentage,
                sumDealMin: response.sumDealMin,
                sumDealMax: response.sumDealMax,
                sumDealJump: response.sumDealJump,
                sumDealSliderValue: response.sumDealSliderValue,
                sumLoanDealMin: response.sumLoanDealMin,
                sumLoanDealJump: response.sumLoanDealJump,
                sumLoanDealSliderValue: response.sumLoanDealSliderValue,
                numOfPaymentMin: response.numOfPaymentMin,
                numOfPaymentMax: response.yearsList[0].maxPay,
                numOfPaymentValue: response.yearsList[0].maxPay,
                minPay: response.minPercentSlider,
                maxPay: response.maxPay,
                minPercentSlider: response.minPercentSlider,
                modalVisible: false,
                show: false,
                privacyHtmlText: response.privacy,
                termsHtmlText: response.terms,
                negishutHtmlText: response.negishut,
                dictionaryTextList: dictionaryResponse,
            }, () => {
                let finance = new Finance();
                //console.log(this.state.yearsList[0].baloonArray)
                this.setState({ rangePayList: this.state.yearsList[0].baloonArray })

                if (this.state.yearsList[0].baloonArray.length === 0) {
                    this.setState({ showBalloonSlider: false }, () => {
                        this.setState({ maxPercentSlider: 0, percentSliderValue: 0 }, () => {
                            this.setState({ endBalloonPeriod: 0 })
                        })
                    })
                } else {
                    this.setState({ showBalloonSlider: true }, () => {
                        this.setState({ maxPercentSlider: this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage, percentSliderValue: this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage }, () => {
                            this.setState({ endBalloonPeriod: parseFloat((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue).toFixed(2)).toLocaleString() }, () => {
                            })
                        })
                    })
                }

                if (response.sumLoanDealSliderValue === 0) {
                    this.setState({
                        maxPercentSlider: 0,
                        percentSliderValue: 0,
                        // numOfPaymentMax: response.yearsList[0].maxPay,
                        endBalloonPeriod: 0
                    }, () => {
                        this.setState({
                            loading: false,
                            monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12),
                            maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue,
                        });
                    });
                } else {
                    this.setState({ maxPercentSlider: (response.rangePayList[response.rangePayList.length - 1].baloonRangePercentage * response.sumDealSliderValue) / response.sumLoanDealSliderValue }, () => {
                        this.setState({
                            loading: false,
                            monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12),
                            maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue,
                            endBalloonPeriod: parseFloat((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue).toFixed(2)).toLocaleString()
                        });
                    })
                }
            })
        } catch (error) {
            console.log('error at getting data from api!')
        }
    }



    calculateMaxPercentSlider = (value) => {

        let finance = new Finance();

        if (this.state.sumLoanDealSliderValue === 0) {
            this.setState({
                maxPercentSlider: 0,
                percentSliderValue: 0
            })
        } else {
            if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                    this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    });
                }
                else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                    this.setState({
                        maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue,
                        percentSliderValue: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue
                    }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    });
                }
                else {
                    this.state.rangePayList.map((item, index) => {
                        if (this.state.numOfPaymentValue >= this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue <= this.state.rangePayList[index].maxRangePay) {
                            this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / value }, () => {
                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (value)) });
                                } else {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (value - (this.state.percentSliderValue / 100 * value))) + ((this.state.percentSliderValue / 100 * value) * this.state.ribit / 12) });
                                }
                            });
                        }
                        return (null);
                    })
                }
            }
        }
    }



    handleChangeStart = () => {
        // console.log('Change event started')
    };

    handleChange = value => {
        let finance = new Finance();

        if (value > this.state.sumDealMax) {
            value = this.state.sumDealMax;
        }

        if (this.state.sumLoanDealSliderValue < this.state.sumLoanDealMin) {
            this.setState({ sumDealSliderValue: value }, () => {
                if (this.state.sumLoanDealSliderValue < this.state.sumLoanDealMin) {
                    console.log('part1 1')

                    this.setState({ sumLoanDealSliderValue: this.state.sumLoanDealMin }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            console.log('part1 2')

                            this.setState({
                                endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue,
                            })
                        } else {
                            console.log('part1 3')

                            this.setState({
                                endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue,
                                maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * value) / this.state.sumLoanDealSliderValue
                            })
                        }
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            console.log('part1 4')

                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            console.log('part1 5')

                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    })
                }
            })
        } else {

            if (this.state.percentSliderValue > this.state.maxPercentSlider) {

                this.setState({ percentSliderValue: this.state.maxPercentSlider }, () => {
                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                    } else {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                })
            } else {
                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            }
            if (this.state.endBalloonPeriod > this.state.maxBalloonInput) {

                this.setState({ endBalloonPeriod: this.state.maxBalloonInput })
            } else {
                this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {

                    this.setState({ endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue })
                    // console.log('!!@!@!@!@!@!@!@!@!@!@')
                    // this.setState({endBalloonPeriod: value}, () => {
                    //   this.setState({percentSliderValue: this.state.endBalloonPeriod*this.state.maxPercentSlider/this.state.maxBalloonInput}, () => {
                    //     });
                    //   })
                })
            }

            if (this.state.sumLoanDealSliderValue === 0) {
                this.setState({
                    maxPercentSlider: 0,
                    percentSliderValue: 0
                })
            } else {
                if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                    if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * value) / this.state.sumLoanDealSliderValue }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * value) / this.state.sumLoanDealSliderValue }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    else {
                        this.state.rangePayList.map((item, index) => {
                            if (this.state.numOfPaymentValue >= this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue <= this.state.rangePayList[index].maxRangePay) {
                                this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * value) / this.state.sumLoanDealSliderValue }, () => {
                                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                    } else {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                    }
                                });
                            }
                            return (null);
                        })
                    }
                }
            }

            if (this.state.sumDealSliderValue > this.state.sumDealMax) {
                this.setState({
                    sumDealSliderValue: this.state.sumDealMax
                }, () => {

                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                    } else {

                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                })
            }
            // else if(this.state.sumDealSliderValue < this.state.sumDealMin){
            //   this.setState({
            //     sumDealSliderValue: this.state.sumDealMin
            //   }, () => {
            //     this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
            //   })
            // }
            else {

                if (this.state.sumLoanDealSliderValue > value) {

                    this.setState({ sumLoanDealSliderValue: value }, () => {

                        this.setState({
                            sumDealSliderValue: value,
                            endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue
                        }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {

                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        })
                    })
                } else {

                    this.setState({
                        sumDealSliderValue: value,
                        endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue
                    }, () => {

                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {

                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    })
                }
            }
        }
    };

    handleChangeComplete = () => {
        // console.log('Change event completed')
        if (this.state.sumLoanDealSliderValue > this.state.sumDealSliderValue) {
            this.setState({ sumLoanDealSliderValue: this.state.sumDealSliderValue })
        }
    };

    handleSumLoanDealChangeStart = () => {
        // console.log('Change event started')
    };

    handleSumLoanDealChange = value => {
        let finance = new Finance();

        // if(this.state.endBalloonPeriod > this.state.maxPercentSlider/100 * this.state.sumLoanDealSliderValue){
        //   this.setState({endBalloonPeriod: this.state.maxPercentSlider/100 * this.state.sumLoanDealSliderValue})
        // }

        if (value > this.state.sumDealSliderValue) {
            value = this.state.sumDealSliderValue;
        }

        if (value === 0) {
            value = this.state.sumLoanDealMin;
        }


        if (value === 0) {
            this.setState({
                maxPercentSlider: 0,
                percentSliderValue: 0,
                sumLoanDealSliderValue: this.state.sumLoanDealMin

            })
        } else {
            if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                    this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    });
                }
                else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                    this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    });
                }
                else {
                    this.state.rangePayList.map((item, index) => {
                        if (this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay) {
                            this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                } else {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                }
                            });
                        }
                        return (null);
                    })
                }
            }
        }

        if (this.state.percentSliderValue > this.state.maxPercentSlider) {
            this.setState({ percentSliderValue: this.state.maxPercentSlider }, () => {
                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            })
        } else {
            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
            } else {

                this.setState({
                    sumLoanDealSliderValue: value
                }, () => {
                    this.setState({ endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue }, () => {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    })
                })
            }
        }
        if (this.state.endBalloonPeriod > this.state.maxBalloonInput) {
            this.setState({ endBalloonPeriod: this.state.maxBalloonInput }, () => {
                if (this.state.endBalloonPeriod > this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue) {
                    this.setState({ endBalloonPeriod: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue })
                }
            }, () => {

                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            })
        } else {
            this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
                this.setState({ endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue })
                // console.log(this.state.endBalloonPeriod)
                // console.log('!!@!@!@!@!@!@!@!@!@!@')
                // this.setState({endBalloonPeriod: value}, () => {
                //   this.setState({percentSliderValue: this.state.endBalloonPeriod*this.state.maxPercentSlider/this.state.maxBalloonInput}, () => {
                //     });
                //   })
            })
        }

        if (value > this.state.sumDealSliderValue) {
            value = this.state.sumDealSliderValue
        }

        if (this.state.sumLoanDealSliderValue > this.state.sumDealSliderValue) {
            this.setState({
                sumLoanDealSliderValue: this.state.sumDealSliderValue
            }, () => {
                if (this.state.sumLoanDealSliderValue === 0) {
                    this.setState({
                        maxPercentSlider: 0,
                        percentSliderValue: 0
                    })
                } else {
                    if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    else {
                        this.state.rangePayList.map((item, index) => {
                            if (this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay) {
                                this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                    } else {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                    }
                                });
                            }
                            return (null);
                        })
                    }
                }
                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            })
        }
        // else if(this.state.sumLoanDealSliderValue < this.state.sumLoanDealMin){
        //   this.setState({
        //     sumLoanDealSliderValue: this.state.sumLoanDealMin
        //   }, () => {
        //     if(this.state.sumLoanDealSliderValue === 0){
        //       this.setState({
        //         maxPercentSlider: 0,
        //         percentSliderValue: 0
        //       })
        //     }else{
        //       if(this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay){
        //         this.setState({maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
        //           this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
        //         });
        //       }
        //       else if(this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length-1].maxRangePay){
        //         this.setState({maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
        //           this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
        //         });
        //       }
        //       else{
        //         this.state.rangePayList.map((item, index) => {
        //           if(this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay){
        //             this.setState({maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
        //               this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
        //             });
        //           }
        //           return(null);
        //         })
        //       }
        //     }
        //     this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
        //   })
        // }
        else {
            this.setState({
                sumLoanDealSliderValue: value,
                endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue
            }, () => {
                if (this.state.sumLoanDealSliderValue === 0) {
                    this.setState({
                        maxPercentSlider: 0,
                        percentSliderValue: 0
                    })
                } else {
                    if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                        if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                            this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                } else {
                                    if (this.state.percentSliderValue > this.state.maxPercentSlider) {
                                        this.setState({ percentSliderValue: this.state.maxPercentSlider }, () => {
                                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) })
                                        })
                                    } else {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) })
                                    }
                                }
                            });
                        }
                        else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                            this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                } else {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                }
                            });
                        }
                        else {
                            this.state.rangePayList.map((item, index) => {
                                if (this.state.numOfPaymentValue >= this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue <= this.state.rangePayList[index].maxRangePay) {
                                    this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / value }, () => {
                                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (value)) });
                                        } else {
                                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (value - (this.state.percentSliderValue / 100 * value))) + ((this.state.percentSliderValue / 100 * value) * this.state.ribit / 12) });
                                        }
                                    });
                                }
                                return (null);
                            })
                        }
                    }
                }
                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            })
        }

        //this.calculateMaxPercentSlider(value);
    };

    handleSumLoanDealChangeComplete = () => {
        // console.log('Change event completed')
    };

    selectHandleChange = (opt) => {
        let finance = new Finance();

        if (opt.balloonArray.length === 0) {
            this.setState({ showBalloonSlider: false }, () => {
                //console.log(this.state.showBalloonSlider);
            })
        } else {
            this.setState({ showBalloonSlider: true }, () => {
                //console.log(this.state.showBalloonSlider);
            })
        }

        this.setState({ rangePayList: opt.balloonArray }, () => {
            if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                if (this.state.sumLoanDealSliderValue === 0) {
                    this.setState({
                        maxPercentSlider: 0,
                        percentSliderValue: 0
                    })
                } else {
                    if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage) }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage) }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    else {
                        this.state.rangePayList.map((item, index) => {
                            if (this.state.numOfPaymentValue >= this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue <= this.state.rangePayList[index].maxRangePay) {
                                this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage) }, () => {
                                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                    } else {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                    }
                                });
                            }
                            return (null);
                        })
                    }
                }

                this.setState({ selectValue: opt }, () => {
                    this.setState({
                        numOfPaymentMax: this.state.selectValue.pay,
                    }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                        if (this.state.numOfPaymentValue > this.state.numOfPaymentMax) {
                            this.setState({ numOfPaymentValue: this.state.numOfPaymentMax }, () => {
                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                } else {
                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                }
                            });
                        }
                    });
                });

                this.setState({ ribit: opt.r / 100 }, () => {
                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                    } else {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                }) //because the ribit is %
                if (opt.isP === false) {
                    if (opt.indexL === true) {
                        this.setState({ ribitLabel: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Ribit")].Value + opt.r + '%' + this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "IndexLinked")].Value }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        })
                    } else {
                        this.setState({ ribitLabel: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Ribit")].Value + opt.r + '%' }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        })
                    }
                } else {
                    this.setState({ ribitLabel: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Prime")].Value + '+ ' + opt.differenceFP + '%' }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    })
                }

                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            } else {
                this.setState({ numOfPaymentMax: opt.pay }, () => {
                    if (this.state.numOfPaymentValue > this.state.numOfPaymentMax) {
                        this.setState({ numOfPaymentValue: this.state.numOfPaymentMax }, () => {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) })
                        })
                    }
                    else {
                        this.setState({ numOfPaymentValue: this.state.numOfPaymentValue }, () => {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) })
                        })
                    }
                }, () => {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) })
                })

                this.setState({ ribit: opt.r / 100 }, () => {
                    console.log(opt.r)
                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                    } else {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                }) //because the ribit is %
                if (opt.isP === false) {
                    if (opt.indexL === true) {
                        this.setState({ ribitLabel: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Ribit")].Value + opt.r + '%' + this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "IndexLinked")].Value }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        })
                    } else {
                        this.setState({ ribitLabel: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Ribit")].Value + opt.r + '%' }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {
                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        })
                    }
                } else {
                    this.setState({ ribitLabel: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Prime")].Value + '+ ' + opt.differenceFP + '%' }, () => {
                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                        } else {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        }
                    })
                }
            }
        });
    }

    handleNumOfPaymentChangeStart = () => {
        // console.log('Change event started')
    };

    handleNumOfPaymentChange = value => { //assign of the maxPercentSlider
        let finance = new Finance();

        if (value === 0) {
            this.setState({ numOfPaymentValue: this.state.numOfPaymentMin }, () => {
                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            })

        } else {
            if (this.state.percentSliderValue > this.state.maxPercentSlider) {
                this.setState({ percentSliderValue: this.state.maxPercentSlider }, () => {

                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                        let monthlyRepayments = -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))
                        this.setState({ monthlyRepayments });
                    } else {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                })

            } else {
                this.state.rangePayList.map((item, index) => {
                    if (value >= this.state.rangePayList[index].minRangePay && value <= this.state.rangePayList[index].maxRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {

                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {

                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, value, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });
                    }
                    return (null);
                })
                // console.log('----------')
                // console.log(this.state.numOfPaymentValue)
                // console.log(this.state.sumLoanDealSliderValue)
                // console.log(ddd)
                // console.log('----------')

                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                } else {



                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, value, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                }
            }

            if (this.state.endBalloonPeriod > this.state.maxBalloonInput) {
                this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
                    this.setState({ endBalloonPeriod: this.state.maxBalloonInput }, () => {
                    })
                })

            } else {
                this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
                    this.setState({ endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue }, () => {

                    }, () => {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, value, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    })
                    // console.log('!!@!@!@!@!@!@!@!@!@!@')
                    // this.setState({endBalloonPeriod: value}, () => {
                    //   this.setState({percentSliderValue: this.state.endBalloonPeriod*this.state.maxPercentSlider/this.state.maxBalloonInput}, () => {
                    //     });
                    //   })
                })
            }

            if (this.state.sumLoanDealSliderValue === 0) {
                this.setState({
                    maxPercentSlider: 0,
                    percentSliderValue: 0
                })
            } else {
                if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                    if (value === this.state.rangePayList[0].minRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                            } else {

                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, value, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                            }
                        });

                    }
                    else if (value === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                        this.setState({
                            maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue,
                            percentSliderValue: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue
                        }, () => {

                            if (this.state.percentSliderValue > 100) {
                                this.setState({ percentSliderValue: 100 }, () => {
                                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                    } else {

                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                    }
                                })
                            }
                        });

                    }
                    else {
                        this.state.rangePayList.map((item, index) => {
                            if (value >= this.state.rangePayList[index].minRangePay && value <= this.state.rangePayList[index].maxRangePay) {
                                this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {

                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                    } else {

                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, value, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                    }
                                });
                            }
                            return (null);
                        })
                    }
                }
            }

            if (this.state.numOfPaymentValue > this.state.numOfPaymentMax) {
                this.setState({
                    numOfPaymentValue: this.state.numOfPaymentMax
                }, () => {

                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                    } else {

                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                });

            }
            else {
                this.setState({
                    numOfPaymentValue: value,
                    endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue
                }, () => {

                    if (this.state.maxPercentSlider < this.state.percentSliderValue) {
                        this.setState({
                            percentSliderValue: this.state.maxPercentSlider
                        }, () => {

                            this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
                                this.setState({ endBalloonPeriod: this.state.maxBalloonInput }, () => {

                                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                    } else {

                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                    }
                                })
                            })
                        })
                    }
                })
            }

            if (this.state.maxPercentSlider < this.state.percentSliderValue) {
                this.setState({
                    percentSliderValue: this.state.maxPercentSlider
                }, () => {

                    if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                    } else {

                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                    }
                });
            }
        }

        // console.log('maxBalloonInput',this.state.maxBalloonInput)
        // console.log('maxPercentSlider',this.state.maxPercentSlider)
        // console.log('sumLoanDealSliderValue',this.state.sumLoanDealSliderValue)
        // console.log('----------------------')

    };

    handleSumLoanDealChangeComplete = () => {
        // console.log('Change event completed')
    };

    handlePercentSliderChangeStart = () => {
        // console.log('Change event started')
    };

    handlePercentSliderChange = value => {
        let finance = new Finance();

        if (this.state.percentSliderValue > this.state.maxPercentSlider) {
            this.setState({ percentSliderValue: this.state.maxPercentSlider }, () => {
                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) })
            })
        } else {
            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) })
        }
        if (this.state.endBalloonPeriod > this.state.maxBalloonInput) {
            this.setState({ endBalloonPeriod: this.state.maxBalloonInput })
        } else {
            this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
                this.setState({ endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue })
                // console.log('!!@!@!@!@!@!@!@!@!@!@')
                // this.setState({endBalloonPeriod: value}, () => {
                //   this.setState({percentSliderValue: this.state.endBalloonPeriod*this.state.maxPercentSlider/this.state.maxBalloonInput}, () => {
                //     });
                //   })
            })
        }

        if (this.state.sumLoanDealSliderValue === 0) { //calculate the maximum of the balloon slider
            this.setState({
                maxPercentSlider: 0,
                percentSliderValue: 0,
            })
        } else {
            if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                    // console.log('max',this.state.maxBalloonInput)
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                });
            }
            else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                    // console.log('max',this.state.maxBalloonInput)
                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                });
            }
            else {
                this.state.rangePayList.map((item, index) => {
                    if (this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay) {
                        this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                            // console.log('max',this.state.maxBalloonInput)
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                        });
                    }
                    return (null);
                })
            }
        }

        this.setState({ percentSliderValue: value }, () => {
            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
            this.setState({ endBalloonPeriod: value == 0 ? 0 : this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue })
            if (value === 0) {
                this.setState({ endBalloonPeriod: 0 })
            } else {
                this.setState({ endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue }, () => {
                    // console.log(this.state.endBalloonPeriod)
                })
            }
        })
    };

    handlePercentSliderChangeComplete = () => {
        // console.log('Change event completed')
    };

    handlePercentInputChange = value => {
        let finance = new Finance();

        if (value > this.state.maxBalloonInput) {
            value = this.state.maxBalloonInput;
        }

        if (value > this.state.maxBalloonInput) {
            this.setState({ endBalloonPeriod: this.state.maxBalloonInput })
        }

        this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
            this.setState({ endBalloonPeriod: value }, () => {
                this.setState({ percentSliderValue: this.state.endBalloonPeriod * this.state.maxPercentSlider / this.state.maxBalloonInput }, () => {
                    if (this.state.percentSliderValue > this.state.maxPercentSlider) {
                        this.setState({ percentSliderValue: this.state.maxPercentSlider }, () => {
                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) })
                        })
                    } else {
                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) })
                    }
                });
            })
        })
    }

    // calculate = () => {
    //   this.setState({
    //     calculateSum: true,
    //     modalVisible: true
    //   });
    //   let finance = new Finance();
    //   this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)}, () => {
    //     console.log('monthly repayments:',this.state.monthlyRepayments);
    //   });
    // }

    closeModal = () => {
        this.setState({ modalShow: false });
    };

    openModal = () => {
        this.setState({ modalShow: true });
    };

    _onBlur1 = () => {
        this.setState({ showDealSumSliderFocus: false })
    }

    _onFocus1 = () => {
        this.setState({ showDealSumSliderFocus: true })
    }

    _onBlur2 = () => {
        this.setState({ showLoanSumSliderFocus: false })
    }

    _onFocus2 = () => {
        this.setState({ showLoanSumSliderFocus: true })
    }

    _onBlur3 = () => {
        this.setState({ showPaymentsSliderFocus: false })
    }

    _onFocus3 = () => {
        this.setState({ showPaymentsSliderFocus: true })
    }

    _onBlur4 = () => {
        this.setState({ showBalloonPercentageSliderFocus: false })
    }

    _onFocus4 = () => {
        this.setState({ maxBalloonInput: this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue }, () => {
        })
        this.setState({ showBalloonPercentageSliderFocus: true })
    }

    numberWithOutCommas = x => {
        return x.replace(/,/g, "").toString()
    };

    render() {
        return (
            <div>
                {this.state.loading ? (
                    <div className='loaderContainer'>
                        <img className="loaderStyle" src={Loader} alt='loader' />
                    </div>
                ) : (
                    <div className='App'>

                        {/* desktop header */}
                        {!isMobile && (
                            <div className='headerInBody'>
                                <img className='logoDesktop' src={logoDesktop} alt="logo"></img>
                                <h1 className='headerText'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CalculatorTitle")].Value}</h1>
                            </div>
                        )}
                        <div className='body'>
                            <div style={{ width: 350 }}>
                                {!isMobile && (
                                    <div className='first' tabIndex='0'>
                                        <div className='flx2'>
                                            <div className='flx2' style={{ marginTop: 2 }}>
                                                <h2 className='headerText5' style={{ marginTop: 17 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CalculateMonthlyRepayment")].Value}</h2>
                                                <h2 className='sumMonthlyRepaymentStyle' style={{ marginTop: -2 }}>{isNaN(Number(this.state.monthlyRepayments)) ? (0) : (this.state.monthlyRepayments.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }))}</h2>
                                                <h2 className='headerText3' style={{ marginTop: -4 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NisPerMounth")].Value}</h2>
                                            </div>
                                            <div className='vl'></div>
                                            <div className='flx2'>
                                                <h2 className='headerText4' style={{ marginBottom: 2 }}>{this.state.ribitLabel}</h2>
                                                <h2 className='headerText4'>{this.state.numOfPaymentValue > this.state.numOfPaymentMax ? (
                                                    this.state.numOfPaymentMax
                                                ) : (
                                                    this.state.numOfPaymentValue
                                                )} {this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PaymentsTitle")].Value}</h2>
                                                {(this.state.endBalloonPeriod === 0) || (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) ? (
                                                    <h2 className='headerText4' style={{ marginTop: 18 }}></h2>
                                                ) : (
                                                    <h2 className='headerText4' style={{ marginTop: -5 }}>₪{isNaN(Number(this.state.endBalloonPeriod)) ?
                                                        (this.state.endBalloonPeriod.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })) :
                                                        ((this.state.endBalloonPeriod > this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue ? (this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }) : this.state.endBalloonPeriod.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })))} {this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "BalloonEndPeriod")].Value}
                                                    </h2>
                                                )}
                                            </div>
                                        </div>
                                    </div>)}

                                <div id='headerId' className='headerStyle' >

                                    {/* mobile header */}
                                    {isMobile && (
                                        <>
                                            <h1 className='headerText'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CalculatorTitle")].Value}</h1>
                                            <img className='logo' src={logo} alt="logo"></img>
                                            <div className='flx first' tabIndex='0'>
                                                <div className='flx2' style={{ paddingTop: 13 }}>
                                                    <h2 className='headerText4' style={{ marginTop: 1 }}>{this.state.ribitLabel}</h2>
                                                    <h2 className='headerText4' style={{ paddingTop: 3 }}>{this.state.numOfPaymentValue > this.state.numOfPaymentMax ? (
                                                        this.state.numOfPaymentMax
                                                    ) : (
                                                        this.state.numOfPaymentValue
                                                    )} {this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PaymentsTitle")].Value}</h2>
                                                    {(this.state.endBalloonPeriod === 0) || (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) ? (
                                                        <h2 className='headerText4' style={{ marginTop: 8 }}></h2>
                                                    ) : (
                                                        <h2 className='headerText4' style={{ marginTop: -5 }}>₪{isNaN(Number(this.state.endBalloonPeriod)) ?
                                                            (this.state.endBalloonPeriod.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })) :
                                                            ((this.state.endBalloonPeriod > this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue ? (this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }) : this.state.endBalloonPeriod.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })))} {this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "BalloonEndPeriod")].Value}
                                                        </h2>
                                                    )}

                                                </div>
                                                <div className='vl'></div>
                                                <div className='flx2' style={{ paddingTop: 26 }}>
                                                    <h2 className='headerText3' style={{ fontWeight: 'bold', marginTop: -3 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CalculateMonthlyRepayment")].Value}</h2>
                                                    <h2 className='sumMonthlyRepaymentStyle'>{isNaN(Number(this.state.monthlyRepayments)) ? (0) : (this.state.monthlyRepayments.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }))}</h2>
                                                    <h2 className='headerText3'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NisPerMounth")].Value}</h2>
                                                </div>
                                            </div>
                                        </>
                                    )}
                                </div>
                            </div>

                            {/* modal */}
                            <Modal
                                show={this.state.modalShow}
                                onHide={this.closeModal}
                                size="lg"
                                aria-labelledby="contained-modal-title-vcenter"
                                centered={true}
                                contentclassname={"modal-content "}
                            >
                                <Modal.Header className='modalHeaderStyle'>

                                    <Modal.Title id="contained-modal-title-vcenter">
                                        {this.state.modalTitle}
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <div className='modalTextStyle'
                                        dangerouslySetInnerHTML={{ __html: this.state.htmlText }}
                                    ></div>

                                </Modal.Body>
                                <Modal.Footer style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                }}>
                                    <Button onClick={this.closeModal}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "Confirm")].Value}</Button>
                                </Modal.Footer>
                            </Modal>

                            <div className='objects'>

                                {/* select car year */}
                                <div
                                    className='selectStyle'
                                //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CarYearTitle")].Value}
                                >
                                    <label
                                        tabIndex='0'
                                        aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CarYearTitle")].Value}
                                        className='fieldSelectTitleStyle'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CarYearTitle")].Value}</label>
                                    <Select
                                        options={this.state.yearsList.map(item => {
                                            return {
                                                value: item.valCode,
                                                label: item.valName,
                                                pay: item.maxPay,
                                                indexL: item.indexLinked,
                                                isP: item.isPrime,
                                                r: item.r,
                                                // p: item.prime,
                                                p: this.state.prime,
                                                differenceFP: item.differenceFromPrime,
                                                balloonArray: item.baloonArray
                                            };
                                        })}

                                        defaultValue={{ label: this.state.yearsList[0].valName, value: this.state.yearsList[0].valCode }}
                                        onChange={this.selectHandleChange}
                                    />
                                </div>

                                {/* mobile car price */}
                                {isMobile && (
                                    <div
                                        className='sliderContainer'
                                    //onBlur={this._onBlur1} 
                                    //onFocus={this._onFocus1} 
                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value} 
                                    //tabIndex='0'
                                    >
                                        <div className='allInput'>
                                            <span className='inputInsideValueType'>₪</span>
                                            <input
                                                tabIndex='0'
                                                //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value}
                                                id='input1'
                                                type='tel'
                                                className='inputSlider'
                                                value={this.state.sumDealSliderValue > this.state.sumDealMax ? (
                                                    this.state.sumDealMax.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),
                                                    this.setState({ sumDealSliderValue: this.state.sumDealMax })
                                                ) : (
                                                    this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                )}
                                                onChange={e => {
                                                    if (isNaN(Number(e.target.value))) {
                                                        this.handleChange(
                                                            e.target.value.replace(/\D/g, "")
                                                        );
                                                    } else {
                                                        if (Number(e.target.value) > this.state.sumDealMax) {
                                                            this.setState({ sumDealSliderValue: this.state.sumDealMax })
                                                        } else {
                                                            this.handleChange(
                                                                Number(e.target.value)
                                                            );
                                                        }
                                                    }
                                                }}
                                                onBlur={e => {
                                                    let finance = new Finance();

                                                    if (+(e.target.value.replace(/[^\d.\-eE+]/g, "")) < this.state.sumDealMin) {
                                                        this.setState({ sumDealSliderValue: this.state.sumDealMin })
                                                    }
                                                    if (this.state.sumLoanDealSliderValue < this.state.sumLoanDealMin) {
                                                        this.setState({ sumLoanDealSliderValue: this.state.sumLoanDealMin }, () => {
                                                            this.setState({ endBalloonPeriod: (this.state.percentSliderValue / 100 * this.state.sumLoanDealMin).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }) });
                                                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                            } else {
                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                            }
                                                        })
                                                    }
                                                }}
                                                onKeyDown={event => {
                                                    if (event.key === "Enter") {
                                                        document.getElementById('input2').focus();
                                                    } else if (event.key === ' ') {
                                                        this.setState({ sumDealSliderValue: this.state.sumDealMin })
                                                    } else if (event.key === 'Backspace') {
                                                        if (this.state.sumDealSliderValue < this.state.sumDealMin) {
                                                            this.setState({ sumDealSliderValue: this.state.sumDealMin })
                                                        }
                                                    }
                                                }}
                                            />
                                        </div>
                                        <label
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value}
                                            className='fieldTitleStyle'
                                            tabIndex='0'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value}</label>
                                        <span
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.sumDealMin + '₪'}
                                            className='minSpan'>{this.state.sumDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</span>
                                        <span
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.sumDealMax + '₪'}
                                            className='maxSpan'>{this.state.sumDealMax.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</span>
                                        <div onBlur={this._onBlur1} onFocus={this._onFocus1}>
                                            <Slider
                                                className={this.state.showDealSumSliderFocus ? ('focus-on-slider-sealSum') : ('')}
                                                min={this.state.sumDealMin}
                                                max={this.state.sumDealMax}
                                                value={this.state.sumDealSliderValue}
                                                onChangeStart={this.handleChangeStart}
                                                onChange={this.handleChange}
                                                onChangeComplete={this.handleChangeComplete}
                                                step={this.state.sumDealJump} //jumps of values              
                                                format={value => <div style={{ fontSize: 11 }}>{value.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                            />
                                        </div>
                                    </div>
                                )}

                                {/* desktop car price */}
                                {!isMobile && (
                                    <div
                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value} 
                                    >
                                        <label
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value}
                                            className='fieldTitleStyle'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value}</label>
                                        <div className='flexSliderInput'>

                                            <div className='allInput'>
                                                <span className='inputInsideValueType'>₪</span>
                                                <input
                                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "DealSumTitle")].Value} 
                                                    //aria-label='₪'
                                                    tabIndex='0'
                                                    className='inputSlider'
                                                    style={{ marginTop: 10, paddingBottom: 8, paddingTop: 6 }}

                                                    value={this.state.sumDealSliderValue > this.state.sumDealMax ? (
                                                        this.state.sumDealMax.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),
                                                        this.setState({ sumDealSliderValue: this.state.sumDealMax })
                                                    ) : (
                                                        this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                    )}

                                                    onChange={e => {
                                                        console.log('onchane**********')
                                                        let sumDealMaxWithoutCommas = this.numberWithOutCommas(e.target.value)

                                                        console.log(sumDealMaxWithoutCommas)
                                                        console.log(typeof this.state.sumDealMax)

                                                        if (isNaN(Number(sumDealMaxWithoutCommas))) {
                                                            this.handleChange(
                                                                Number(sumDealMaxWithoutCommas)
                                                            );
                                                        } else {
                                                            if (Number(sumDealMaxWithoutCommas) > this.state.sumDealMax) {
                                                                this.setState({ sumDealSliderValue: this.state.sumDealMax })
                                                            } else {
                                                                this.handleChange(
                                                                    Number(sumDealMaxWithoutCommas)
                                                                );
                                                            }
                                                        }
                                                    }}

                                                    onKeyDown={event => {
                                                        if (event.keyCode === 32 || event.keyCode === 8) {
                                                            this.setState({ sumDealSliderValue: this.state.sumDealMin })
                                                        }
                                                    }}

                                                    onBlur={e => {
                                                        console.log('onblur**********')

                                                        let finance = new Finance();

                                                        if (+(e.target.value.replace(/[^\d.\-eE+]/g, "")) < this.state.sumDealMin) {
                                                            this.setState({ sumDealSliderValue: this.state.sumDealMin })
                                                        }
                                                        if (this.state.sumLoanDealSliderValue < this.state.sumLoanDealMin) {
                                                            this.setState({ sumLoanDealSliderValue: this.state.sumLoanDealMin }, () => {
                                                                this.setState({ endBalloonPeriod: (this.state.percentSliderValue / 100 * this.state.sumLoanDealMin).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }) });
                                                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                                } else {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                }
                                                            })
                                                        }
                                                    }}
                                                />
                                            </div>
                                            <div className='sliderContainer'>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.sumDealMin + '₪'}
                                                    className='minSpan'>{this.state.sumDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</span>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.sumDealMax + '₪'}
                                                    className='maxSpan'>{this.state.sumDealMax.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</span>
                                                <div onBlur={this._onBlur1} onFocus={this._onFocus1}>
                                                    <Slider
                                                        className={this.state.showDealSumSliderFocus ? ('focus-on-slider-sealSum') : ('')}
                                                        min={this.state.sumDealMin}
                                                        max={this.state.sumDealMax}
                                                        value={this.state.sumDealSliderValue}
                                                        onChangeStart={this.handleChangeStart}
                                                        onChange={this.handleChange}
                                                        onChangeComplete={this.handleChangeComplete}
                                                        step={this.state.sumDealJump} //jumps of values            
                                                        format={value => <div style={{ fontSize: 11 }}>{value.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}

                                {/* mobile loan sum deal */}
                                {isMobile && (
                                    <div
                                        className='sliderContainer'
                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value} 
                                    //tabIndex='0'
                                    >
                                        <div className='allInput'>
                                            <span className='inputInsideValueType' style={{ marginTop: 10 }}>₪</span>
                                            <input
                                                tabIndex='0'
                                                //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value}
                                                id='input2'
                                                type='tel'
                                                className='inputSlider'
                                                style={{ marginTop: 11 }}

                                                value={this.state.sumLoanDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}

                                                // value={this.state.sumLoanDealSliderValue > this.state.sumDealSliderValue ? (
                                                //   this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),
                                                //   this.setState({sumLoanDealSliderValue: this.state.sumDealSliderValue})
                                                //   ) : (
                                                //     this.state.sumLoanDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                //   )}

                                                onChange={e => {
                                                    console.log(e.target.value)

                                                    if (isNaN(+(e.target.value.replace(/[^\d.\-eE+]/g, "")))) {
                                                        this.handleSumLoanDealChange(
                                                            e.target.value.replace(/\D/g, "")
                                                        );
                                                    } else {
                                                        this.handleSumLoanDealChange(
                                                            +(e.target.value.replace(/[^\d.\-eE+]/g, ""))
                                                        );
                                                    }

                                                    // let finance = new Finance();

                                                    // if (isNaN(+(e.target.value.replace(/[^\d.\-eE+]/g, "")))) {
                                                    //   this.handleSumLoanDealChange(
                                                    //     e.target.value.replace(/\D/g, "")
                                                    //   );
                                                    // } else {
                                                    //   if(+(e.target.value.replace(/[^\d.\-eE+]/g, "")) >= this.state.sumDealSliderValue){
                                                    //     this.setState({sumLoanDealSliderValue: this.state.sumDealSliderValue}, () => {
                                                    //       this.setState({endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue}, () => {
                                                    //           if(this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0){
                                                    //             if(this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay){
                                                    //               this.setState({maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                    //                 this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //               });
                                                    //             }
                                                    //             else if(this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length-1].maxRangePay){
                                                    //               this.setState({
                                                    //                 maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue,
                                                    //                 percentSliderValue: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue
                                                    //               }, () => {
                                                    //                 this.setState({endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue})
                                                    //                 this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //               });
                                                    //             }
                                                    //             else{
                                                    //               this.state.rangePayList.map((item, index) => {
                                                    //                 if(this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay){
                                                    //                   this.setState({maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                    //                     this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //                   });
                                                    //                 }
                                                    //                 return(null);
                                                    //               })
                                                    //             }
                                                    //           }else{
                                                    //             this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))});
                                                    //           }
                                                    //         })
                                                    //       if(!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0){
                                                    //         this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))});
                                                    //       }else{
                                                    //         this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //       }
                                                    //     })
                                                    //   } else{
                                                    //     this.setState({sumLoanDealSliderValue: e.target.value}, () => {
                                                    //       if(this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0){
                                                    //         if(this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay){
                                                    //           this.setState({maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                    //             this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //           });
                                                    //         }
                                                    //         else if(this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length-1].maxRangePay){
                                                    //           this.setState({maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                    //             this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //           });
                                                    //         }
                                                    //         else{
                                                    //           this.state.rangePayList.map((item, index) => {
                                                    //             if(this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay){
                                                    //               this.setState({maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                    //                 this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                    //               });
                                                    //             }
                                                    //             return(null);
                                                    //           })
                                                    //         }
                                                    //       }else{
                                                    //         this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))});
                                                    //       }
                                                    //     })
                                                    //     this.handleSumLoanDealChange(
                                                    //       +(e.target.value.replace(/[^\d.\-eE+]/g, ""))
                                                    //     );
                                                    //   }
                                                    // }
                                                }}
                                                onBlur={e => {
                                                    let finance = new Finance();
                                                    if (+(e.target.value.replace(/[^\d.\-eE+]/g, "")) < this.state.sumLoanDealMin) {
                                                        this.setState({
                                                            sumLoanDealSliderValue: this.state.sumLoanDealMin,
                                                            endBalloonPeriod: (this.state.percentSliderValue / 100 * this.state.sumLoanDealMin).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })
                                                        }, () => {
                                                            if (this.state.sumLoanDealSliderValue === 0) {
                                                                this.setState({
                                                                    maxPercentSlider: 0,
                                                                    percentSliderValue: 0
                                                                })
                                                            } else {

                                                                if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                                                                    if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                                                                        this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                                                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                        });
                                                                    }
                                                                    else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                                                                        this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                                                            this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                        });
                                                                    }
                                                                    else {
                                                                        this.state.rangePayList.map((item, index) => {
                                                                            if (this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay) {
                                                                                this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                                });
                                                                            }
                                                                            return (null);
                                                                        })
                                                                    }
                                                                } else {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }}
                                                onKeyPress={event => {
                                                    if (event.key === "Enter") {
                                                        document.getElementById('input3').focus();
                                                    }
                                                }}
                                            />
                                        </div>
                                        <label
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value}
                                            className='fieldTitleStyle'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value}</label>
                                        <span
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.sumLoanDealMin + '₪'}
                                            className='minSpan'>{this.state.sumLoanDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</span>
                                        <span
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.sumDealSliderValue + '₪'}
                                            className='maxSpan'>{(this.state.sumDealSliderValue < this.state.sumLoanDealMin) ? (this.state.sumLoanDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')) : (this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'))}</span>
                                        <div onBlur={this._onBlur2} onFocus={this._onFocus2}>
                                            <Slider
                                                className={this.state.showLoanSumSliderFocus ? ('focus-on-slider-loanSum') : ('')}
                                                min={this.state.sumLoanDealMin}
                                                max={this.state.sumDealSliderValue}
                                                value={this.state.sumLoanDealSliderValue}
                                                onChangeStart={this.handleSumLoanDealChangeStart}
                                                onChange={this.handleSumLoanDealChange}
                                                onChangeComplete={this.handleSumLoanDealChangeComplete}
                                                step={this.state.sumLoanDealJump} //jumps of values
                                                format={value => <div style={{ fontSize: 11 }}>{value.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                            />
                                        </div>
                                    </div>
                                )}

                                {/* desktop loan sum deal */}
                                {!isMobile && (
                                    <div
                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value} 
                                    >
                                        <label
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value}
                                            className='fieldTitleStyle' style={{ paddingTop: 0, marginTop: -20 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value}</label>
                                        <div className='flexSliderInput'>

                                            <div className='allInput'>
                                                <span className='inputInsideValueType' style={{ marginTop: 4 }}>₪</span>
                                                <input
                                                    tabIndex='0'
                                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "LoanSumTitle")].Value}
                                                    className='inputSlider'
                                                    style={{ marginTop: 9, paddingBottom: 8, paddingTop: 6 }}

                                                    value={this.state.sumLoanDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}

                                                    // value={this.state.sumLoanDealSliderValue > this.state.sumDealSliderValue ? (
                                                    //   this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),
                                                    //   this.setState({sumLoanDealSliderValue: this.state.sumDealSliderValue})
                                                    //   ) : (
                                                    //     this.state.sumLoanDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                    //   )}

                                                    // value={ this.state.sumLoanDealSliderValue < this.state.sumLoanDealMin ? (
                                                    //   null
                                                    //   // this.state.sumLoanDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                    //   // this.setState({sumLoanDealSliderValue: this.state.sumLoanDealMin})
                                                    //   ) : (
                                                    //     this.state.sumLoanDealSliderValue > this.state.sumDealSliderValue ? (
                                                    //       this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                    //       // this.setState({sumLoanDealSliderValue: this.state.sumDealSliderValue})
                                                    //     ) : (
                                                    //       this.state.sumLoanDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
                                                    //     ))}  

                                                    onChange={e => {
                                                        let sumDealMaxWithoutCommas = this.numberWithOutCommas(e.target.value)


                                                        if (isNaN(sumDealMaxWithoutCommas)) {
                                                            this.handleSumLoanDealChange(
                                                                Number(sumDealMaxWithoutCommas)
                                                            );
                                                        } else {
                                                            this.handleSumLoanDealChange(
                                                                Number(sumDealMaxWithoutCommas)
                                                            );
                                                        }
                                                        // let finance = new Finance();

                                                        // if (isNaN(+(e.target.value.replace(/[^\d.\-eE+]/g, "")))) {
                                                        //   this.handleSumLoanDealChange(
                                                        //     e.target.value.replace(/\D/g, "")
                                                        //   );
                                                        // } else {
                                                        //   if(+(e.target.value.replace(/[^\d.\-eE+]/g, "")) >= this.state.sumDealSliderValue){
                                                        //     this.setState({sumLoanDealSliderValue: this.state.sumDealSliderValue}, () => {
                                                        //       this.setState({endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue}, () => {
                                                        //           if(this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0){
                                                        //             if(this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay){
                                                        //               this.setState({maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                        //                 this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //               });
                                                        //             }
                                                        //             else if(this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length-1].maxRangePay){
                                                        //               this.setState({
                                                        //                 maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue,
                                                        //                 percentSliderValue: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue
                                                        //               }, () => {
                                                        //                 this.setState({endBalloonPeriod: this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue})
                                                        //                 this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //               });
                                                        //             }
                                                        //             else{
                                                        //               this.state.rangePayList.map((item, index) => {
                                                        //                 if(this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay){
                                                        //                   this.setState({maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                        //                     this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //                   });
                                                        //                 }
                                                        //                 return(null);
                                                        //               })
                                                        //             }
                                                        //           }else{
                                                        //             this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))});
                                                        //           }
                                                        //         })
                                                        //       if(!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0){
                                                        //         this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))});
                                                        //       }else{
                                                        //         this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //       }
                                                        //     })
                                                        //   } else{
                                                        //     this.setState({sumLoanDealSliderValue: e.target.value}, () => {
                                                        //       if(this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0){
                                                        //         if(this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay){
                                                        //           this.setState({maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                        //             this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //           });
                                                        //         }
                                                        //         else if(this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length-1].maxRangePay){
                                                        //           this.setState({maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length-1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                        //             this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //           });
                                                        //         }
                                                        //         else{
                                                        //           this.state.rangePayList.map((item, index) => {
                                                        //             if(this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay){
                                                        //               this.setState({maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue}, () => {
                                                        //                 this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue-(this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)))+((this.state.percentSliderValue/100*this.state.sumLoanDealSliderValue)*this.state.ribit/12)});
                                                        //               });
                                                        //             }
                                                        //             return(null);
                                                        //           })
                                                        //         }
                                                        //       }else{
                                                        //         this.setState({monthlyRepayments: -finance.PMT(this.state.ribit/12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue))});
                                                        //       }
                                                        //     })
                                                        //     this.handleSumLoanDealChange(
                                                        //       +(e.target.value.replace(/[^\d.\-eE+]/g, ""))
                                                        //     );
                                                        //   }
                                                        // }
                                                    }}

                                                    onBlur={e => {
                                                        let finance = new Finance();

                                                        if (+(e.target.value.replace(/[^\d.\-eE+]/g, "")) < this.state.sumLoanDealMin) {
                                                            this.setState({
                                                                sumLoanDealSliderValue: this.state.sumLoanDealMin,
                                                                endBalloonPeriod: (this.state.percentSliderValue / 100 * this.state.sumLoanDealMin).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }),
                                                            }, () => {
                                                                if (this.state.sumLoanDealSliderValue === 0) {
                                                                    this.setState({
                                                                        maxPercentSlider: 0,
                                                                        percentSliderValue: 0
                                                                    })
                                                                } else {
                                                                    if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                                                                        if (this.state.numOfPaymentValue === this.state.rangePayList[0].minRangePay) {
                                                                            this.setState({ maxPercentSlider: (this.state.rangePayList[0].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                            });
                                                                        }
                                                                        else if (this.state.numOfPaymentValue === this.state.rangePayList[this.state.rangePayList.length - 1].maxRangePay) {
                                                                            this.setState({ maxPercentSlider: (this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                            });
                                                                        }
                                                                        else {
                                                                            this.state.rangePayList.map((item, index) => {
                                                                                if (this.state.numOfPaymentValue > this.state.rangePayList[index].minRangePay && this.state.numOfPaymentValue < this.state.rangePayList[index].maxRangePay) {
                                                                                    this.setState({ maxPercentSlider: (this.state.rangePayList[index].baloonRangePercentage * this.state.sumDealSliderValue) / this.state.sumLoanDealSliderValue }, () => {
                                                                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                                    });
                                                                                }
                                                                                return (null);
                                                                            })
                                                                        }
                                                                    } else {
                                                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    }}
                                                />
                                            </div>
                                            <div className='sliderContainer'>

                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.sumLoanDealMin + '₪'}
                                                    className='minSpan'>{this.state.sumLoanDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</span>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.sumDealSliderValue + '₪'}
                                                    className='maxSpan'>{(this.state.sumDealSliderValue < this.state.sumLoanDealMin) ? (this.state.sumLoanDealMin.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')) : (this.state.sumDealSliderValue.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'))}</span>
                                                <div onBlur={this._onBlur2} onFocus={this._onFocus2}>
                                                    <Slider
                                                        className={this.state.showLoanSumSliderFocus ? ('focus-on-slider-loanSum') : ('')}
                                                        min={this.state.sumLoanDealMin}
                                                        max={this.state.sumDealSliderValue}
                                                        value={this.state.sumLoanDealSliderValue}
                                                        onChangeStart={this.handleSumLoanDealChangeStart}
                                                        onChange={this.handleSumLoanDealChange}
                                                        onChangeComplete={this.handleSumLoanDealChangeComplete}
                                                        step={this.state.sumLoanDealJump} //jumps of values
                                                        format={value => <div style={{ fontSize: 11 }}>{value.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}

                                {/* mobile num of payments */}
                                {isMobile && (
                                    <div
                                        className='sliderContainer'
                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value} 
                                    //tabIndex='0'
                                    >
                                        <div className='allInput'>
                                            <span
                                                className='inputInsideValueType'
                                                style={{ marginTop: 10 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}</span>
                                            <input
                                                tabIndex='0'
                                                //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value}
                                                id='input3'
                                                type='tel'
                                                className='inputSlider'
                                                style={{ marginTop: 11 }}
                                                value={this.state.numOfPaymentValue > this.state.numOfPaymentMax ? (
                                                    this.state.numOfPaymentMax
                                                ) : (
                                                    this.state.numOfPaymentValue
                                                )}
                                                onChange={e => {
                                                    let finance = new Finance();

                                                    if (isNaN(Number(e.target.value))) {
                                                        //this.handleNumOfPaymentChange(
                                                        // e.target.value.replace(/\D/g, "")
                                                        //);
                                                        this.setState({ numOfPaymentValue: '', monthlyRepayments: 0 })
                                                    } else {
                                                        if (Number(e.target.value) > this.state.numOfPaymentMax) {
                                                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                                this.setState({
                                                                    numOfPaymentValue: this.state.numOfPaymentMax,
                                                                }, () => {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                                })
                                                            } else {
                                                                this.setState({
                                                                    numOfPaymentValue: this.state.numOfPaymentMax,
                                                                    maxPercentSlider: this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage
                                                                }, () => {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                })
                                                            }
                                                        } else {
                                                            this.handleNumOfPaymentChange(
                                                                Number(e.target.value)
                                                            );
                                                        }
                                                    }
                                                }}
                                                onBlur={e => {
                                                    let finance = new Finance();

                                                    if (Number(e.target.value) < this.state.numOfPaymentMin) {
                                                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                            this.setState({
                                                                numOfPaymentValue: this.state.numOfPaymentMin
                                                            }, () => {
                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                            })
                                                        } else {
                                                            this.setState({
                                                                numOfPaymentValue: this.state.numOfPaymentMin,
                                                                maxPercentSlider: this.state.rangePayList[0].baloonRangePercentage
                                                            }, () => {
                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                            })
                                                        }
                                                    }
                                                    if (Number(e.target.value) === this.state.numOfPaymentMin) {
                                                        if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                                                            this.setState({
                                                                numOfPaymentValue: this.state.numOfPaymentMin,
                                                                maxPercentSlider: this.state.rangePayList[0].baloonRangePercentage
                                                            }, () => {
                                                                this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                            })
                                                        }
                                                    }

                                                }}
                                                onKeyPress={event => {
                                                    if (event.key === "Enter") {
                                                        if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                            document.getElementById('input4').focus();
                                                        } else {
                                                            document.getElementById('input1').focus();
                                                        }
                                                    }
                                                }}
                                            />
                                        </div>
                                        <label
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value}
                                            className='fieldTitleStyle'>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value}</label>
                                        <span
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.numOfPaymentMin +
                                                this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}
                                            className='minSpan'>{this.state.numOfPaymentMin}</span>
                                        <span
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.numOfPaymentMax +
                                                this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}
                                            className='maxSpan'>{this.state.numOfPaymentMax}</span>
                                        <div onBlur={this._onBlur3} onFocus={this._onFocus3}>
                                            <Slider
                                                className={this.state.showPaymentsSliderFocus ? ('focus-on-slider-Payments') : ('')}
                                                min={this.state.numOfPaymentMin}
                                                max={this.state.numOfPaymentMax}
                                                value={this.state.numOfPaymentValue}
                                                onChangeStart={this.handleNumOfPaymentChangeStart}
                                                onChange={this.handleNumOfPaymentChange}
                                                onChangeComplete={this.handleSumLoanDealChangeComplete}
                                                step={this.state.numOfPaymentJump} //jumps of values
                                                format={value => <div style={{ fontSize: 11 }}>{value.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                            />
                                        </div>
                                    </div>
                                )}

                                {/* desktop num of payments */}
                                {!isMobile && (
                                    <div
                                    //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value} 
                                    >
                                        <label
                                            tabIndex='0'
                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value}
                                            className='fieldTitleStyle'
                                            style={{ paddingTop: 0, marginTop: -20 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "ChooseNumOfPayments")].Value}</label>
                                        <div className='flexSliderInput'>

                                            <div className='allInput'>
                                                <span
                                                    className='inputInsideValueType'
                                                    style={{ marginTop: 4 }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}</span>
                                                <input
                                                    title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}
                                                    className='inputSlider'
                                                    style={{ marginTop: 9, paddingBottom: 8, paddingTop: 6 }}
                                                    value={this.state.numOfPaymentValue > this.state.numOfPaymentMax ? (
                                                        this.state.numOfPaymentMax
                                                    ) : (
                                                        this.state.numOfPaymentValue
                                                    )}
                                                    onChange={e => {
                                                        let finance = new Finance();

                                                        if (isNaN(Number(e.target.value))) {
                                                            //this.handleNumOfPaymentChange(
                                                            // e.target.value.replace(/\D/g, "")
                                                            //);
                                                            this.setState({ numOfPaymentValue: '', monthlyRepayments: 0 })
                                                        } else {
                                                            if (Number(e.target.value) > this.state.numOfPaymentMax) {
                                                                if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                                    this.setState({
                                                                        numOfPaymentValue: this.state.numOfPaymentMax,
                                                                    }, () => {
                                                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                                    })
                                                                } else {
                                                                    this.setState({
                                                                        numOfPaymentValue: this.state.numOfPaymentMax,
                                                                        maxPercentSlider: this.state.rangePayList[this.state.rangePayList.length - 1].baloonRangePercentage
                                                                    }, () => {
                                                                        this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                    })
                                                                }
                                                            } else {
                                                                this.handleNumOfPaymentChange(
                                                                    Number(e.target.value)
                                                                );
                                                            }
                                                        }
                                                    }}
                                                    onBlur={e => {
                                                        let finance = new Finance();

                                                        if (Number(e.target.value) < this.state.numOfPaymentMin) {
                                                            if (!this.state.showBalloonSlider || this.state.numOfPaymentValue < this.state.minPay || this.state.numOfPaymentValue > this.state.maxPay || this.state.maxPercentSlider === 0) {
                                                                this.setState({
                                                                    numOfPaymentValue: this.state.numOfPaymentMin
                                                                }, () => {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue)) });
                                                                })
                                                            } else {
                                                                this.setState({
                                                                    numOfPaymentValue: this.state.numOfPaymentMin,
                                                                    maxPercentSlider: this.state.rangePayList[0].baloonRangePercentage
                                                                }, () => {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                })
                                                            }
                                                        }
                                                        if (Number(e.target.value) === this.state.numOfPaymentMin) {
                                                            if (this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) {
                                                                this.setState({
                                                                    numOfPaymentValue: this.state.numOfPaymentMin,
                                                                    maxPercentSlider: this.state.rangePayList[0].baloonRangePercentage
                                                                }, () => {
                                                                    this.setState({ monthlyRepayments: -finance.PMT(this.state.ribit / 12, this.state.numOfPaymentValue, (this.state.sumLoanDealSliderValue - (this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue))) + ((this.state.percentSliderValue / 100 * this.state.sumLoanDealSliderValue) * this.state.ribit / 12) });
                                                                })
                                                            }
                                                        }


                                                    }}
                                                />
                                            </div>
                                            <div className='sliderContainer'>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.numOfPaymentMin +
                                                        this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}
                                                    className='minSpan'>{this.state.numOfPaymentMin}</span>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.numOfPaymentMax +
                                                        this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MounthsLabel")].Value}
                                                    className='maxSpan'>{this.state.numOfPaymentMax}</span>
                                                <div>
                                                    <div onBlur={this._onBlur3} onFocus={this._onFocus3}>
                                                        <Slider
                                                            className={this.state.showPaymentsSliderFocus ? ('focus-on-slider-Payments') : ('')}
                                                            min={this.state.numOfPaymentMin}
                                                            max={this.state.numOfPaymentMax}
                                                            value={this.state.numOfPaymentValue}
                                                            onChangeStart={this.handleNumOfPaymentChangeStart}
                                                            onChange={this.handleNumOfPaymentChange}
                                                            onChangeComplete={this.handleSumLoanDealChangeComplete}
                                                            step={this.state.numOfPaymentJump} //jumps of values
                                                            format={value => <div style={{ fontSize: 11 }}>{value.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}

                                {(this.state.showBalloonSlider && this.state.numOfPaymentValue >= this.state.minPay && this.state.numOfPaymentValue <= this.state.maxPay && this.state.maxPercentSlider !== 0) ? (
                                    <div>

                                        {/* mobile balloon sum */}
                                        {isMobile && (
                                            <div
                                                className='sliderContainer sliderContainerPercent'
                                            //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value} 
                                            //tabIndex='0'
                                            >
                                                <div className='allInput'>

                                                    <span className='inputInsideValueType' style={{ marginTop: 9 }}>₪</span>
                                                    {/* {console.log('zubi mobile',this.state.endBalloonPeriod,this.state)} */}
                                                    <input
                                                        tabIndex='0'
                                                        //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value}
                                                        id='input4'
                                                        type='tel'
                                                        className='inputSlider'
                                                        style={{ marginTop: 10 }}

                                                        //value={this.state.endBalloonPeriod.toLocaleString(undefined, {minimumFractionDigits: 0, maximumFractionDigits: 0})}

                                                        value={this.state.endBalloonPeriod > this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue ?
                                                            (this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })
                                                            : (this.state.endBalloonPeriod.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }))
                                                        }

                                                        // value={isNaN(Number(this.state.endBalloonPeriod)) ? 
                                                        //     (this.state.endBalloonPeriod) : ( parseInt(this.state.endBalloonPeriod) > parseInt(this.state.maxBalloonInput) ? (
                                                        //       this.state.maxBalloonInput.toLocaleString(undefined, {minimumFractionDigits: 0, maximumFractionDigits: 0}),
                                                        //       this.setState({endBalloonPeriod: this.state.maxBalloonInput})
                                                        //       ) : (
                                                        //         this.state.endBalloonPeriod.toLocaleString(undefined, {minimumFractionDigits: 0, maximumFractionDigits: 0})
                                                        //         )
                                                        //     )}

                                                        onChange={e => {
                                                            if (isNaN(Number(e.target.value))) {
                                                                this.handlePercentInputChange(
                                                                    e.target.value.replace(/\D/g, "")
                                                                );
                                                            } else {
                                                                if (Number(e.target.value) > this.state.maxBalloonInput) {
                                                                    this.setState({ endBalloonPeriod: this.state.maxBalloonInput }, () => {
                                                                        this.handlePercentInputChange(
                                                                            Number(this.state.endBalloonPeriod)
                                                                        );
                                                                    })
                                                                } else {
                                                                    this.handlePercentInputChange(
                                                                        Number(e.target.value)
                                                                    );
                                                                }
                                                            }
                                                        }}
                                                        onBlur={e => {
                                                            if (Number(e.target.value) < this.state.minPercentSlider) {
                                                                this.setState({ percentSliderValue: this.state.minPercentSlider })
                                                            }
                                                        }}
                                                        onKeyPress={event => {
                                                            if (event.key === "Enter") {
                                                                document.getElementById('input1').focus();
                                                            }
                                                        }}
                                                    />
                                                </div>
                                                <label
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value + this.state.percentSliderValue + this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PercentTitle")].Value}
                                                    className='fieldTitleStyle'>
                                                    <span
                                                        style={{ color: 'rgba(0, 56, 99, 0.5)' }}>({(this.state.maxBalloonInput === 0 && this.state.percentSliderValue === 0) ? (0) : (parseInt(this.state.percentSliderValue) > parseInt(this.state.maxPercentSlider) ? (this.state.maxPercentSlider.toFixed(0).toString().replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,")) : (this.state.percentSliderValue.toFixed(0).toString().replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,")))}%)</span> {this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value}</label>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.minPercentSlider +
                                                        this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PercentTitle")].Value}
                                                    className='minSpan'>{this.state.minPercentSlider}%</span>
                                                <span
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.maxPercentSlider +
                                                        this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PercentTitle")].Value}
                                                    className='maxSpan'>{this.state.maxPercentSlider.toFixed(0).toString().replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,")}%</span>
                                                <div onBlur={this._onBlur4} onFocus={this._onFocus4}>
                                                    <Slider
                                                        className={this.state.showBalloonPercentageSliderFocus ? ('focus-on-slider-balloonPercentage') : ('')}
                                                        min={this.state.minPercentSlider}
                                                        max={this.state.maxPercentSlider > 100 ? (
                                                            100,
                                                            this.setState({ maxPercentSlider: 100 })
                                                        ) : (
                                                            this.state.maxPercentSlider)} //check the range by the previous slider
                                                        value={this.state.percentSliderValue}
                                                        onChangeStart={this.handlePercentSliderChangeStart}
                                                        onChange={this.handlePercentSliderChange}
                                                        onChangeComplete={this.handlePercentSliderChangeComplete}
                                                        step={this.state.numOfPaymentJump} //jumps of values
                                                        format={value => <div style={{ fontSize: 11 }}>{value.toFixed(0).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                                    />
                                                </div>
                                            </div>
                                        )}

                                        {/* desktop balloon sum */}
                                        {!isMobile && (
                                            <div
                                            //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value} 
                                            >
                                                <label
                                                    tabIndex='0'
                                                    aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value + this.state.percentSliderValue + this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PercentTitle")].Value}
                                                    className='fieldTitleStyle'
                                                    style={{ paddingTop: 0, marginTop: -22 }}>
                                                    <span
                                                        style={{ color: 'rgba(0, 56, 99, 0.5)' }}>({this.state.maxBalloonInput === 0 ? (0) : (parseInt(this.state.percentSliderValue) > parseInt(this.state.maxPercentSlider) ? (this.state.maxPercentSlider.toFixed(0).toString().replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,")) : (this.state.percentSliderValue.toFixed(0).toString().replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,")))}%)</span> {this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value}</label>
                                                <div className='flexSliderInput'>
                                                    <div className='allInput'>

                                                        <span className='inputInsideValueType'>₪</span>
                                                        {/* {console.log('zubi desktop',this.state.endBalloonPeriod)} */}

                                                        <input
                                                            tabIndex='0'
                                                            //title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "balloonPercentageTitle")].Value}
                                                            className='inputSlider'
                                                            style={{ marginTop: 11, paddingBottom: 8, paddingTop: 6 }}

                                                            //value={this.state.endBalloonPeriod.toLocaleString(undefined, {minimumFractionDigits: 0, maximumFractionDigits: 0})}

                                                            value={this.state.endBalloonPeriod > this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue ?
                                                                (this.state.maxPercentSlider / 100 * this.state.sumLoanDealSliderValue).toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 })
                                                                : (this.state.endBalloonPeriod.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 }))}

                                                            // value={isNaN(Number(this.state.endBalloonPeriod)) ? 
                                                            //   (this.state.endBalloonPeriod) : ( parseInt(this.state.endBalloonPeriod) > parseInt(this.state.maxBalloonInput) ? (
                                                            //     this.state.maxBalloonInput.toLocaleString(undefined, {minimumFractionDigits: 0, maximumFractionDigits: 0}),
                                                            //     this.setState({endBalloonPeriod: this.state.maxBalloonInput})
                                                            //     ) : (
                                                            //       this.state.endBalloonPeriod.toLocaleString(undefined, {minimumFractionDigits: 0, maximumFractionDigits: 0})
                                                            //       )
                                                            //   )}

                                                            onChange={e => {
                                                                if (isNaN(Number(e.target.value))) {
                                                                    this.handlePercentInputChange(
                                                                        e.target.value.replace(/\D/g, "")
                                                                    );
                                                                } else {
                                                                    if (Number(e.target.value) > this.state.maxBalloonInput) {
                                                                        this.setState({ endBalloonPeriod: this.state.maxBalloonInput }, () => {
                                                                            this.handlePercentInputChange(
                                                                                Number(this.state.endBalloonPeriod)
                                                                            );
                                                                        })
                                                                    } else {
                                                                        this.handlePercentInputChange(
                                                                            Number(e.target.value)
                                                                        );
                                                                    }
                                                                }
                                                            }}
                                                            onBlur={e => {
                                                                if (Number(e.target.value) < this.state.minPercentSlider) {
                                                                    this.setState({
                                                                        percentSliderValue: this.state.minPercentSlider
                                                                    })
                                                                }
                                                            }}
                                                        />
                                                    </div>
                                                    <div className='sliderContainer sliderContainerPercent'>
                                                        <span
                                                            tabIndex='0'
                                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MinimumTitle")].Value + this.state.minPercentSlider +
                                                                this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PercentTitle")].Value}
                                                            className='minSpan'>{this.state.minPercentSlider}%</span>
                                                        <span
                                                            tabIndex='0'
                                                            aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "MaximumTitle")].Value + this.state.maxPercentSlider +
                                                                this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PercentTitle")].Value}
                                                            className='maxSpan'>{this.state.maxPercentSlider.toFixed(0).toString().replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,")}%</span>
                                                        <div onBlur={this._onBlur4} onFocus={this._onFocus4}>
                                                            <Slider
                                                                className={this.state.showBalloonPercentageSliderFocus ? ('focus-on-slider-balloonPercentage') : ('')}
                                                                min={this.state.minPercentSlider}
                                                                max={this.state.maxPercentSlider > 100 ? (
                                                                    100,
                                                                    this.setState({ maxPercentSlider: 100 })
                                                                ) : (
                                                                    this.state.maxPercentSlider)} //check the range by the previous slider
                                                                value={this.state.percentSliderValue}
                                                                onChangeStart={this.handlePercentSliderChangeStart}
                                                                onChange={this.handlePercentSliderChange}
                                                                onChangeComplete={this.handlePercentSliderChangeComplete}
                                                                step={this.state.numOfPaymentJump} //jumps of values
                                                                format={value => <div style={{ fontSize: 11 }}>{value.toFixed(0).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')}</div>}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )}

                                    </div>
                                ) : (null)}

                                {/* mobile links */}
                                {!isMobile &&
                                    (<label
                                        tabIndex='0'
                                        aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CreateNewLoan")].Value}
                                        className='fieldSelectTitleStyle'
                                        style={{ paddingRight: 30, color: '#121E32' }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CreateNewLoan")].Value}</label>)
                                }
                            </div>
                            {isMobile && (
                                <>
                                    {isMobile && (
                                        <>
                                            <label
                                                tabIndex='0'
                                                aria-label={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CreateNewLoan")].Value}
                                                className='fieldSelectTitleStyle'
                                                style={{ paddingRight: 38, color: '#121E32' }}>{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "CreateNewLoan")].Value}</label>
                                            <div className='balloonSliderBorderStyle'></div>
                                        </>
                                    )}
                                    <div className='linkContainer'>
                                        <a
                                            title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PressHereNegishutTitle")].Value}
                                            className='popupLink'
                                            // style={{paddingLeft: '50%'}}
                                            tabIndex='0'
                                            onKeyDown={event => {
                                                if (event.keyCode === 13) {
                                                    this.setState({
                                                        htmlText: this.state.negishutHtmlText,
                                                        modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NegishutTitle")].Value
                                                    }, () => {
                                                        this.openModal();
                                                    })
                                                }
                                            }}
                                            onClick={() => {
                                                this.setState({
                                                    htmlText: this.state.negishutHtmlText,
                                                    modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NegishutTitle")].Value
                                                }, () => {
                                                    this.openModal();
                                                })
                                            }}
                                        >{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NegishutTitle")].Value}</a>

                                        <h5 className='popupLink seperator'>|</h5>

                                        <a
                                            title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PressHerePrivacyTitle")].Value}
                                            className='popupLink'
                                            tabIndex='0'
                                            onKeyDown={event => {
                                                if (event.keyCode === 13) {
                                                    this.setState({
                                                        htmlText: this.state.privacyHtmlText,
                                                        modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PrivacyTitle")].Value
                                                    }, () => {
                                                        this.openModal();
                                                    })
                                                }
                                            }}
                                            onClick={() => {
                                                this.setState({
                                                    htmlText: this.state.privacyHtmlText,
                                                    modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PrivacyTitle")].Value
                                                }, () => {
                                                    this.openModal();
                                                })
                                            }}
                                        >{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PrivacyTitle")].Value}</a>
                                        <h5 className='popupLink seperator'>|</h5>
                                        <a
                                            title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PressHereTermsTitle")].Value}
                                            className='popupLink'
                                            tabIndex='0'
                                            onKeyDown={event => {
                                                if (event.keyCode === 13) {
                                                    this.setState({
                                                        htmlText: this.state.termsHtmlText,
                                                        modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "TermsTitle")].Value
                                                    }, () => {
                                                        this.openModal();
                                                    })
                                                }
                                            }}
                                            onClick={() => {
                                                this.setState({
                                                    htmlText: this.state.termsHtmlText,
                                                    modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "TermsTitle")].Value
                                                }, () => {
                                                    this.openModal();
                                                })
                                            }}
                                        >{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "TermsTitle")].Value}</a>
                                    </div>
                                </>
                            )}
                        </div>

                        {/* desktop links */}
                        {!isMobile && (
                            <>
                                <div className='linkContainer'>
                                    <a
                                        title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PressHereNegishutTitle")].Value}
                                        className='popupLink'
                                        // style={{paddingLeft: '50%'}}
                                        tabIndex='0'
                                        onKeyDown={event => {
                                            if (event.keyCode === 13) {
                                                this.setState({
                                                    htmlText: this.state.negishutHtmlText,
                                                    modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NegishutTitle")].Value
                                                }, () => {
                                                    this.openModal();
                                                })
                                            }
                                        }}
                                        onClick={() => {
                                            this.setState({
                                                htmlText: this.state.negishutHtmlText,
                                                modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NegishutTitle")].Value
                                            }, () => {
                                                this.openModal();
                                            })
                                        }}
                                    >{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "NegishutTitle")].Value}</a>

                                    <h5 className='popupLink seperator'>|</h5>

                                    <a
                                        title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PressHerePrivacyTitle")].Value}
                                        className='popupLink'
                                        // style={{paddingLeft: '50%'}}
                                        tabIndex='0'
                                        onKeyDown={event => {
                                            if (event.keyCode === 13) {
                                                this.setState({
                                                    htmlText: this.state.privacyHtmlText,
                                                    modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PrivacyTitle")].Value
                                                }, () => {
                                                    this.openModal();
                                                })
                                            }
                                        }}
                                        onClick={() => {
                                            this.setState({
                                                htmlText: this.state.privacyHtmlText,
                                                modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PrivacyTitle")].Value
                                            }, () => {
                                                this.openModal();
                                            })
                                        }}
                                    >{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PrivacyTitle")].Value}</a>

                                    <h5 className='popupLink seperator'>|</h5>

                                    <a
                                        title={this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "PressHereTermsTitle")].Value}
                                        className='popupLink'
                                        // style={{paddingRight: '50%'}}
                                        tabIndex='0'
                                        onKeyDown={event => {
                                            if (event.keyCode === 13) {
                                                this.setState({
                                                    htmlText: this.state.termsHtmlText,
                                                    modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "TermsTitle")].Value
                                                }, () => {
                                                    this.openModal();
                                                })
                                            }
                                        }}
                                        onClick={() => {
                                            this.setState({
                                                htmlText: this.state.termsHtmlText,
                                                modalTitle: this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "TermsTitle")].Value
                                            }, () => {
                                                this.openModal();
                                            })
                                        }}
                                    >{this.state.dictionaryTextList[this.state.dictionaryTextList.findIndex(x => x.Key === "TermsTitle")].Value}</a>

                                </div>
                            </>
                        )}
                    </div>
                )}
            </div>
        );
    }
}

export default App;
